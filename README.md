# Bitbucket Pipeline Monitor Widget

This KDE widget monitors the selected Bitbucket Pipelines and shows a notification popup when they finish.

## How to install

???

## How to build

    $ virtualenv pipeline-monitor-widget
    $ pip install -r requirements.txt
    $ python main.py
