import unittest

import httpretty

from src.client.bitbucket_client import getAllPipelines


class BitbucketClientTest(unittest.TestCase):

    @httpretty.activate
    def test_shouldFetchPipelinesUntilFirstEmptyResponse(self):
        with open('tests/test-data/pipelines_response.json', 'rt') as fd:
            pipelinesResponse = fd.read()
        with open('tests/test-data/pipelines_response_empty.json', 'rt') as fd:
            pipelinesEmptyResponse = fd.read()
        httpretty.register_uri(
            method=httpretty.GET,
            uri="https://bitbucket.org/!api/2.0/repositories/keycomponent/example-repo/pipelines/?fields=values.repository.name,values.state.name,values.target.commit.hash,values.target.ref_name,values.creator.display_name,page,size,values.run_number,values.build_number&page=1&pagelen=100",
            body=pipelinesResponse,
            match_querystring=True,
            content_type='application/json'
        )
        httpretty.register_uri(
            method=httpretty.GET,
            uri=f"https://bitbucket.org/!api/2.0/repositories/keycomponent/example-repo/pipelines/?fields=values.repository.name,values.state.name,values.target.commit.hash,values.target.ref_name,values.creator.display_name,page,size,values.run_number,values.build_number&page=2&pagelen=100",
            body=pipelinesEmptyResponse,
            match_querystring=True,
            content_type='application/json'
        )

        pipelines = getAllPipelines('example-repo', 'testuser', 'testpassword')

        self.assertEqual(len(pipelines), 10)
        self.assertEqual(httpretty.last_request().querystring['page'][0], '2')


if __name__ == '__main__':
    unittest.main()
