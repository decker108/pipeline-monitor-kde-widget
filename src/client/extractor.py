
def extractLatestPipeline(pipelinesJson):
    latestBuildNo = max([pl['build_number'] for pl in pipelinesJson['values']])
    return [pl for pl in pipelinesJson['values'] if pl['build_number'] == latestBuildNo]


def extractUnfinishedPipelines(pipelines):
    return [pipeline for pipeline in pipelines if pipeline['state']['name'] == 'IN_PROGRESS']
