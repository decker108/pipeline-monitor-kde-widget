from base64 import b64encode

import requests

bitbucketUrl = 'https://bitbucket.org/!api'


def getAppPassword():
    with open('app_pw', 'rt') as fd:  # TODO configure through in-app options, store in OS-specific location
        output = fd.readline().replace('\n', '')
        return output


def getAppUsername():
    return 'olarende_dw'  # TODO configure through in-app options, store in OS-specific location


def getAllPipelines(repoSlug, username, appPassword):
    result = []

    fields = ','.join([
        'values.repository.name',
        'values.state.name',
        'values.target.commit.hash',
        'values.target.ref_name',
        'values.creator.display_name',
        'page',
        'size',
        'values.run_number',
        'values.build_number',
    ])
    authToken = username + ':' + b64encode(bytes(appPassword, 'ascii'))
    headers = {
        'Content-Type': 'application/json',
        'Authentication': f'Basic {authToken}'
    }
    for page in range(1, 20):  # A more correct way would be to do a while(true), but this is safer.
        url = f"{bitbucketUrl}/2.0/repositories/keycomponent/{repoSlug}/pipelines/?fields={fields}&page={page}&pagelen=100"
        response = requests.get(url, headers=headers, auth=(username, appPassword))
        if response.status_code == 200:
            responseJson = response.json()
            if len(responseJson['values']) > 0:
                result = result + responseJson['values']
            else:
                break

    return result
