from time import sleep
from PyQt5.QtCore import QThread, pyqtSignal

from src.client.bitbucket_client import getAllPipelines, getAppPassword, getAppUsername
from src.client.extractor import extractUnfinishedPipelines


class WorkerThread(QThread):
    signal = pyqtSignal('PyQt_PyObject')

    def __init__(self):
        QThread.__init__(self)

    def run(self):
        appPassword = getAppPassword()
        watchedRepos = ['magento-email-unsubscription']  # TODO configure through in-app options
        username = getAppUsername()

        while True:
            try:
                pipelines = []
                for repoSlug in watchedRepos:
                    pipelines = extractUnfinishedPipelines(getAllPipelines(repoSlug, username, appPassword))

                # TODO determine if pipelines list contain new entries before emitting signal
                if len(pipelines):
                    self.signal.emit(f'Found {len(pipelines)} running pipelines for magento-email-unsubscription',
                                     len(pipelines))

                sleep(10)
            except Exception as e:
                print(f'Error: {e.__class__.__name__}')
                pass
