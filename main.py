import sys
import platform

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from src.background_worker import worker

app = QApplication([])
app.setQuitOnLastWindowClosed(False)
app.setApplicationName('Bitbucket Pipelines monitor')
app.setApplicationVersion('0.0.1-alpha')
tray = QSystemTrayIcon()
if not tray.isSystemTrayAvailable():
    print('Error: System does not support a system tray')
    sys.exit(-1)
tray.setIcon(QIcon('icons/icon_0.png'))
tray.setVisible(True)
tray.setToolTip('Bitbucket Pipelines monitor')


def showNotification(msg):
    msgTitle = 'Pipeline monitor'
    if platform.system() == 'Linux':
        tray.showMessage(msgTitle, msg, QSystemTrayIcon.Information, 3000)
    elif platform.system() == 'Darwin':
        import subprocess
        subprocess.run(['osascript', '-e', f'display notification "{msg}" with title "{msgTitle}"'])


menu = QMenu()
# noinspection PyArgumentList
action = QAction('Quit', triggered=qApp.quit)
menu.addAction(action)
tray.setContextMenu(menu)


def finished(result, numActivePipelines):
    print(f'work finished, result: {result}')
    showNotification(result)
    if numActivePipelines < 10:
        tray.setIcon(QIcon(f'icons/icon_{numActivePipelines}.png'))
    else:
        tray.setIcon(QIcon('icons/icon_max.png'))


workerThread = worker.WorkerThread()
workerThread.signal.connect(finished)

workerThread.start()
app.exec_()  # this line blocks while the app runs and returns on exit
